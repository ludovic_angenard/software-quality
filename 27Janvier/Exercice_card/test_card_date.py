import unittest
import card

class TestFonc(unittest.TestCase):
    symbol_list = "\\","[","=","%","^","]","-","|","®","™","÷","×","[","]","³","£","ß","«","»","©","@","{","}","µ","«","»","±","~","¡","^","°","`","•","´","˜","¨","¤","!","»","#","$","%","&","(",")","=","?","*","~","{","}",":","_",">","@","’","^","[","]","²","³","<",",",".","-"]
    def test_date_symbol(self):
        # the valid symbol is a /
        self.assertTrue(card.check_date('11/20'))
        for symbol_list in symbol:
            self.assertFalse(card.check_date('11'+symbol+'20'))


    def test_month_size(self):
        # No more than 2 number in the month
        self.assertTrue(card.check_date('05/01'))
        self.assertFalse(card.check_date('005/01'))
        self.assertFalse(card.check_date('5/01'))
        self.assertFalse(card.check_date('/01'))

    def test_month_value(self):
        #valid month number
        self.assertTrue(card.check_date('01/20'))
        self.assertTrue(card.check_date('02/20'))
        self.assertTrue(card.check_date('03/20'))
        self.assertTrue(card.check_date('04/20'))
        self.assertTrue(card.check_date('05/20'))
        self.assertTrue(card.check_date('06/20'))
        self.assertTrue(card.check_date('07/20'))
        self.assertTrue(card.check_date('08/20'))
        self.assertTrue(card.check_date('09/20'))
        self.assertTrue(card.check_date('10/20'))
        self.assertTrue(card.check_date('11/20'))
        self.assertTrue(card.check_date('12/20'))
        # Lower than 1 are false
        self.assertFalse(card.check_date('00/20'))
        # negative number are false
        self.assertFalse(card.check_date('-01/20'))
        # Higher than 13 are false
        self.assertFalse(card.check_date('13/20'))

    def test_year_size(self):
        self.assertTrue(card.check_date('05/01'))
        # No more than 2 number in the year
        self.assertFalse(card.check_date('05/001'))
        self.assertFalse(card.check_date('05/1'))
        self.assertFalse(card.check_date('05/'))

    def test_year_value(self):
        self.assertTrue(card.check_date('09/20'))
        # Higher than 2023 are false
        self.assertFalse(card.check_date('09/24'))
        # Lower than 2019 are false
        self.assertFalse(card.check_date('09/18'))


if __name__ == '__main__':
    unittest.main()